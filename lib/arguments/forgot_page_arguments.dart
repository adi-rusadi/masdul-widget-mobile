import 'package:flutter/material.dart';

class ForgotPageArguments<T> {
  final Color color;
  final bool isPhone;
  final String? errorMessage;
  final String label;
  final String submitLabel;
  final void Function({required String emailOrPhone})
      onSubmit;
  final Widget logo;

  ForgotPageArguments({
    required this.color,
    required this.isPhone,
    required this.onSubmit,
    required this.logo,
    required this.label,
    required this.submitLabel,
    required this.errorMessage,
  });
}
