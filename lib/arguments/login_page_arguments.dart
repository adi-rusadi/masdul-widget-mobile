import 'package:flutter/material.dart';

class LoginPageArguments<T> {
  final Color color;
  final String? usernameLabel;
  final String? passwordLabel;
  final String submitLabel;
  final String? registerLabel;
  final String? forgotLabel;
  final String? errorMessage;
  final void Function() onForgot;
  final void Function() onRegister;
  final void Function({required String username, required String password})
      onSubmit;
  final Widget logo;

  LoginPageArguments({
    required this.errorMessage,
    required this.color,
    required this.usernameLabel,
    required this.passwordLabel,
    required this.submitLabel,
    required this.registerLabel,
    required this.forgotLabel,
    required this.onForgot,
    required this.onRegister,
    required this.onSubmit,
    required this.logo,
  });
}
