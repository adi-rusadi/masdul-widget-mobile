import '../models/menu_widget_model.dart';

class MenuWidgetArguments<T> {
  final MenuWidgetHeader? header;
  final List<MenuWidgetList<T>> list;

  MenuWidgetArguments({
    this.header,
    required this.list,
  });
}

class MenuWidgetIconArguments<T> {
  final MenuWidgetHeader? header;

  double? containerWidth;
  final int maxItemInRow;
  final double itemWidth;
  final List<MenuWidgetIconList<T>> list;

  MenuWidgetIconArguments({
    this.header,
    required this.list,
    this.containerWidth,
    required this.itemWidth,
    required this.maxItemInRow,
  });
}
