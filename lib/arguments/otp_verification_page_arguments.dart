import 'package:flutter/material.dart';

class OtpVerificationPageArguments<T> {
  final Color color;
  final String? errorMessage;
  final String label;
  final String hint;
  final String submitLabel;
  final TextInputType keyboardType;
  final void Function({required String otp})
      onSubmit;
  final Widget logo;

  OtpVerificationPageArguments({
    required this.color,
    required this.onSubmit,
    required this.logo,
    required this.label,
    required this.hint,
    required this.keyboardType,
    required this.submitLabel,
    required this.errorMessage,
  });
}
