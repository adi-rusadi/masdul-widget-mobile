import 'package:flutter/material.dart';

class RegisterPageArguments<T> {
  final Color color;
  final String? usernameLabel;
  final String? passwordLabel;
  final String submitLabel;
  final String? errorMessage;
  final List<RegisterExtendField>? extendFields;

  /// extendData key return its label,
  /// if type is textField or dropdown, the value will be string
  /// if type is radio / checkbox, the value will Map<String,dynamic>
  final void Function(
      {required String username,
      required String password,
      required Map<String, dynamic> extendData}) onSubmit;
  final Widget logo;

  RegisterPageArguments({
    required this.errorMessage,
    required this.color,
    required this.usernameLabel,
    required this.passwordLabel,
    required this.submitLabel,
    required this.onSubmit,
    required this.logo,
    this.extendFields,
  });
}

class RegisterExtendField {
  final String label;

  RegisterExtendField({required this.label});
}

class RegisterExtendTextField extends RegisterExtendField {
  final TextInputType keyboardType;
  final TextEditingController controller;
  final bool obscureText;
  RegisterExtendTextField({
    required super.label,
    required this.keyboardType,
    required this.controller,
    required this.obscureText,
  });
}

class RegisterExtendDropdown extends RegisterExtendField {
  final List<DropdownMenuItem> items;
  RegisterExtendDropdown({
    required super.label,
    required this.items,
  });
}

class RegisterExtendRadio extends RegisterExtendField {
  final Map<String, dynamic> items;
  RegisterExtendRadio({required super.label, required this.items});
}

class RegisterExtendCheck extends RegisterExtendField {
  final Map<String, dynamic> items;
  RegisterExtendCheck({required super.label, required this.items});
}
