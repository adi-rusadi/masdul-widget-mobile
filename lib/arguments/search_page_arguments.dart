import 'dart:ui';

import 'package:flutter/material.dart';

import '../models/search_page_widget_model.dart';

class SearchPageArguments<T> {
  final List<SearchPageWidgetModel<T>> data;
  final Color color;
  final String? title;
  final String? hint;
  final Function(String text, List<T> localSearch) onTextChanged;
  final Function(T? value)? onSelected;
  final bool isLocalSearch;

  SearchPageArguments({
    required this.data,
    required this.color,
    required this.title,
    required this.hint,
    required this.onTextChanged,
    this.onSelected,
    this.isLocalSearch = false,
  });
}
