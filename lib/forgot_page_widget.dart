// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:masdul_widget/masdul_widget.dart' as dul;

import 'arguments/forgot_page_arguments.dart';

class ForgotPageWidget<T> extends StatelessWidget {
  final ForgotPageArguments<T> args;
  ForgotPageWidget({super.key, required this.args});
  TextEditingController emailOrPhoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 350,
      // height: 500,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Card(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(height: 20),
              Container(
                width: double.infinity,
                alignment: Alignment.center,
                child: args.logo,
              ),
              const SizedBox(height: 20),
              if (args.errorMessage != null)
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  margin: const EdgeInsets.only(bottom: 10),
                  width: double.infinity,
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                    color: Colors.redAccent,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Text(
                    args.errorMessage!,
                    style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          fontSize: 14,
                          color: Colors.white,
                        ),
                  ),
                ),
              dul.TextFieldWidget(
                controller: emailOrPhoneController,
                hint: args.label,
                keyboardType: args.isPhone
                    ? TextInputType.phone
                    : TextInputType.emailAddress,
              ),
              const SizedBox(height: 10),
              Container(
                // alignment: Alignment.centerRight,
                margin: const EdgeInsets.only(bottom: 20),
                child: dul.ButtonWidget(
                  width: double.infinity,
                  text: args.submitLabel,
                  background: args.color,
                  borderColor: args.color,
                  onPressed: () {
                    args.onSubmit(
                      emailOrPhone: emailOrPhoneController.text,
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
