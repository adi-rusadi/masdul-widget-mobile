// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:masdul_widget/masdul_widget.dart' as dul;

import 'arguments/login_page_arguments.dart';

class LoginPageWidget<T> extends StatelessWidget {
  final LoginPageArguments<T> args;
  LoginPageWidget({super.key, required this.args});
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 350,
      // height: 500,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Card(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(height: 20),
              Container(
                width: double.infinity,
                alignment: Alignment.center,
                child: args.logo,
              ),
              const SizedBox(height: 20),
              if (args.errorMessage != null)
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  margin: const EdgeInsets.only(bottom: 10),
                  width: double.infinity,
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                    color: Colors.redAccent,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Text(
                    args.errorMessage!,
                    style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          fontSize: 14,
                          color: Colors.white,
                        ),
                  ),
                ),
              dul.TextFieldWidget(
                controller: usernameController,
                hint: args.usernameLabel,
                keyboardType: TextInputType.emailAddress,
              ),
              const SizedBox(height: 10),
              dul.TextFieldWidget(
                controller: passwordController,
                hint: args.passwordLabel,
                keyboardType: TextInputType.visiblePassword,
                obscureText: true,
              ),
              const SizedBox(height: 10),
              if (args.forgotLabel != null)
                Container(
                  alignment: Alignment.centerRight,
                  margin: const EdgeInsets.only(bottom: 10),
                  child: InkWell(
                    onTap: args.onForgot,
                    child: Text(
                      args.forgotLabel!,
                      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                            fontSize: 14,
                            color: args.color,
                          ),
                    ),
                  ),
                ),
              Container(
                // alignment: Alignment.centerRight,
                margin: const EdgeInsets.only(bottom: 20),
                child: dul.ButtonWidget(
                  width: double.infinity,
                  text: args.submitLabel,
                  background: args.color,
                  borderColor: args.color,
                  onPressed: () {
                    args.onSubmit(
                      username: usernameController.text,
                      password: passwordController.text,
                    );
                  },
                ),
              ),
              if (args.registerLabel != null)
                Container(
                  // alignment: Alignment.centerRight,
                  margin: const EdgeInsets.only(bottom: 20),
                  child: InkWell(
                    onTap: args.onRegister,
                    child: Text(
                      args.registerLabel!,
                      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                            fontSize: 14,
                            color: args.color,
                          ),
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
