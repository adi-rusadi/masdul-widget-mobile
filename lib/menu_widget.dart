import 'package:flutter/material.dart';
import 'package:masdul_widget/masdul_widget.dart';

import 'arguments/menu_widget_arguments.dart';

class MenuDrawerWidget<T> extends StatelessWidget {
  final MenuWidgetArguments<T> args;
  const MenuDrawerWidget({super.key, required this.args});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: SafeArea(
          child: Column(
            children: [
              if (args.header != null)
                Container(
                  height: 100,
                  color: args.header!.color,
                  child: Center(
                    child: InkWell(
                      onTap: args.header?.onTap,
                      child: ListTile(
                        title: args.header!.title == null
                            ? null
                            : Text(
                                args.header!.title!,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge!
                                    .copyWith(
                                      fontSize: 20,
                                    ),
                              ),
                        subtitle: args.header!.description == null
                            ? null
                            : Text(args.header!.description!),
                        leading: args.header?.leading,
                        trailing: args.header?.trailing,
                      ),
                    ),
                  ),
                ),
              const SizedBox(height: 10),
              Expanded(
                child: ListView.separated(
                  itemCount: args.list.length,
                  itemBuilder: (context, index) {
                    var d = args.list[index];
                    return InkWell(
                      onTap: d.onTap == null
                          ? null
                          : () {
                              d.onTap!(d.value);
                            },
                      child: ListTile(
                        leading: d.leading,
                        trailing: d.trailing,
                        title: d.title == null
                            ? null
                            : Text(
                                d.title!,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge!
                                    .copyWith(
                                      fontSize: 16,
                                    ),
                              ),
                        subtitle:
                            d.description == null ? null : Text(d.description!),
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return const Divider();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MenuBottomSheetWidget<T> extends StatelessWidget {
  final MenuWidgetArguments<T> args;
  const MenuBottomSheetWidget({super.key, required this.args});

  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
      expand: false,
      builder: (BuildContext context, ScrollController scrollController) {
        return ListView.separated(
          controller: scrollController,
          itemCount: args.list.length + 2,
          itemBuilder: (context, index) {
            if (index == 0) {
              return Container(
                width: double.infinity,
                padding: const EdgeInsets.only(top: 5, bottom: 10),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: args.header == null ? null : args.header!.color,
                  borderRadius:
                      const BorderRadius.vertical(top: Radius.circular(10)),
                ),
                child: Column(
                  children: [
                    Container(
                      width: 100,
                      color: Colors.grey,
                      height: 2,
                    ),
                    const SizedBox(height: 2),
                    Container(
                      width: 100,
                      color: Colors.grey,
                      height: 2,
                    ),
                  ],
                ),
              );
            }
            if (index == 1) {
              if (args.header != null) {
                return Container(
                  height: 100,
                  color: args.header!.color,
                  child: Center(
                    child: InkWell(
                      onTap: args.header?.onTap,
                      child: ListTile(
                        title: args.header!.title == null
                            ? null
                            : Text(
                                args.header!.title!,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge!
                                    .copyWith(
                                      fontSize: 20,
                                    ),
                              ),
                        subtitle: args.header!.description == null
                            ? null
                            : Text(args.header!.description!),
                        leading: args.header?.leading,
                        trailing: args.header?.trailing,
                      ),
                    ),
                  ),
                );
              }
            }
            var d = args.list[index - 2];
            return InkWell(
              onTap: d.onTap == null
                  ? null
                  : () {
                      d.onTap!(d.value);
                    },
              child: ListTile(
                leading: d.leading,
                trailing: d.trailing,
                title: d.title == null
                    ? null
                    : Text(
                        d.title!,
                        style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                              fontSize: 16,
                            ),
                      ),
                subtitle: d.description == null ? null : Text(d.description!),
              ),
            );
          },
          separatorBuilder: (BuildContext context, int index) {
            return index == 0 ? Container() : const Divider();
          },
        );
      },
    );
  }
}

class MenuIconWidget<T> extends StatelessWidget {
  final MenuWidgetIconArguments<T> args;
  const MenuIconWidget({super.key, required this.args});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var width = args.containerWidth ?? size.width;
    double space = 0;
    if (args.list.length == 1) //total item == 1
    {
      space = 0;
    } else if (args.list.length == 2) {
      space = width - (args.itemWidth * 2);
    } else {
      var totalSpaceInRow = args.maxItemInRow - 1;
      var spaceWidth = width - (args.itemWidth * args.maxItemInRow);
      space = spaceWidth / totalSpaceInRow;
    }
    if (space < 0) space = 0;

    var length = args.list.length;
    var row = args.maxItemInRow;
    var collumn = length ~/ row;
    var mod = length % row;
    if (mod > 0) {
      collumn++;
    }
    List<Widget> rows = [];
    for (var i = 0; i < collumn; i++) {
      var item = row;
      if (i + 1 == collumn) {
        if (mod > 0) {
          item = mod;
        }
      }
      List<Widget> w = [];
      for (var j = 0; j < item; j++) {
        var index = ((i * (row - 1)) + j);
        if (i != 0) {
          index++;
        }
        var data = args.list[index];
        bool isLast = j == item - 1;
        w.add(
          Container(
            width: args.itemWidth,
            margin: EdgeInsets.only(
              right: isLast ? 0 : space,
            ),
            child: InkWell(
              onTap: () {
                if (data.onTap != null) data.onTap!(data.value);
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  RoundedImage(
                    radius: data.imageRadius,
                    size: args.itemWidth,
                    image: data.image,
                  ),
                  const SizedBox(height: 5),
                  Text(
                    data.title ?? "",
                    style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          fontSize: 12,
                        ),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                    // maxLines: 2,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
        );
      }
      var r = Container(
        padding: const EdgeInsets.only(bottom: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: w,
        ),
      );
      rows.add(r);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(
          height: 100,
          // color: args.header!.color,
          child: Center(
            child: InkWell(
              onTap: args.header?.onTap,
              child: ListTile(
                contentPadding: EdgeInsets.zero,
                title: args.header!.title == null
                    ? null
                    : Text(
                        args.header!.title!,
                        style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                              fontSize: 20,
                            ),
                      ),
                subtitle: args.header!.description == null
                    ? null
                    : Text(args.header!.description!),
                leading: args.header?.leading,
                trailing: args.header?.trailing,
              ),
            ),
          ),
        ),
        const SizedBox(height: 10),
        ...rows.map((e) => e).toList(),
      ],
    );
  }
}
