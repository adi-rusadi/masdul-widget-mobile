import 'package:flutter/material.dart';

class MenuWidgetHeader {
  final String? title;
  final String? description;
  final Color? color;
  final Widget? trailing;
  final Widget? leading;
  final void Function()? onTap;

  MenuWidgetHeader({
    this.title,
    this.description,
    this.color,
    this.leading,
    this.trailing,
    this.onTap,
  });
}

class MenuWidgetList<T> {
  final String? title;
  final String? description;
  final Widget? trailing;
  final Widget? leading;
  final T? value;
  final void Function(T? value)? onTap;

  MenuWidgetList({
    this.title,
    this.description,
    this.trailing,
    this.leading,
    this.onTap,
    this.value,
  });
}

class MenuWidgetIconList<T> {
  final String? title;
  final Image image;
  final double imageRadius;
  final T? value;
  final void Function(T? value)? onTap;

  MenuWidgetIconList({
    this.title,
    required this.image,
    required this.imageRadius,
    this.onTap,
    this.value,
  });
}
