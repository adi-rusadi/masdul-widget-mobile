import 'package:flutter/material.dart';

class SearchPageWidgetModel<T> {
  final T value;
  final String title;
  final String? description;
  final Widget? leading;
  final Widget? trailing;

  SearchPageWidgetModel({
    required this.value,
    required this.title,
    this.description,
    this.leading,
    this.trailing,
  });
}
