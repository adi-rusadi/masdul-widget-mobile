// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:masdul_widget/masdul_widget.dart' as dul;

import 'arguments/otp_verification_page_arguments.dart';

class OtpVerificationPageWidget<T> extends StatelessWidget {
  final OtpVerificationPageArguments<T> args;
  OtpVerificationPageWidget({super.key, required this.args});
  TextEditingController otpController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 350,
      // height: 500,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Card(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(height: 20),
              Container(
                width: double.infinity,
                alignment: Alignment.center,
                child: args.logo,
              ),
              const SizedBox(height: 20),
              if (args.errorMessage != null)
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  margin: const EdgeInsets.only(bottom: 10),
                  width: double.infinity,
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                    color: Colors.redAccent,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Text(
                    args.errorMessage!,
                    style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          fontSize: 14,
                          color: Colors.white,
                        ),
                  ),
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  margin: const EdgeInsets.only(bottom: 10),
                  width: double.infinity,
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                    color: args.color.withOpacity(0.8),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Text(
                    args.label,
                    style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          fontSize: 14,
                          color: Colors.white,
                        ),
                  ),
                ),
              const SizedBox(height: 10),
              dul.TextFieldWidget(
                controller: otpController,
                hint: args.hint,
                keyboardType: args.keyboardType,
                enableCopyPaste: false,
              ),
              Container(
                // alignment: Alignment.centerRight,
                margin: const EdgeInsets.only(bottom: 20),
                child: dul.ButtonWidget(
                  width: double.infinity,
                  text: args.submitLabel,
                  background: args.color,
                  borderColor: args.color,
                  onPressed: () {
                    args.onSubmit(
                      otp: otpController.text,
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
