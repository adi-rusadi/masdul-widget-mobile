// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:masdul_widget/masdul_widget.dart' as dul;

import 'arguments/register_page_arguments.dart';

class RegisterPageWidget<T> extends StatefulWidget {
  final RegisterPageArguments<T> args;
  const RegisterPageWidget({super.key, required this.args});

  @override
  State<RegisterPageWidget<T>> createState() => _RegisterPageWidgetState<T>();
}

class _RegisterPageWidgetState<T> extends State<RegisterPageWidget<T>> {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  Map<String, dynamic> extendData = {};

  @override
  void initState() {
    extendData = {};
    super.initState();
    if (widget.args.extendFields != null) {
      for (var element in widget.args.extendFields!) {
        extendData[element.label] = null;
      }
    }
  }

  @override
  void didUpdateWidget(covariant RegisterPageWidget<T> oldWidget) {
    super.didUpdateWidget(oldWidget);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 350,
      // height: 500,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Card(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(height: 20),
              Container(
                width: double.infinity,
                alignment: Alignment.center,
                child: widget.args.logo,
              ),
              const SizedBox(height: 20),
              if (widget.args.errorMessage != null)
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  margin: const EdgeInsets.only(bottom: 10),
                  width: double.infinity,
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                    color: Colors.redAccent,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Text(
                    widget.args.errorMessage!,
                    style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          fontSize: 14,
                          color: Colors.white,
                        ),
                  ),
                ),
              dul.TextFieldWidget(
                controller: usernameController,
                hint: widget.args.usernameLabel,
                keyboardType: TextInputType.emailAddress,
              ),
              const SizedBox(height: 10),
              dul.TextFieldWidget(
                controller: passwordController,
                hint: widget.args.passwordLabel,
                keyboardType: TextInputType.visiblePassword,
                obscureText: true,
              ),
              const SizedBox(height: 10),
              if (widget.args.extendFields != null)
                SizedBox(
                  width: double.infinity,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: widget.args.extendFields!.map<Widget>((e) {
                      return renderExtend(e);
                    }).toList(),
                  ),
                ),
              Container(
                // alignment: Alignment.centerRight,
                margin: const EdgeInsets.only(bottom: 20),
                child: dul.ButtonWidget(
                  width: double.infinity,
                  text: widget.args.submitLabel,
                  background: widget.args.color,
                  borderColor: widget.args.color,
                  onPressed: () {
                    widget.args.onSubmit(
                        username: usernameController.text,
                        password: passwordController.text,
                        extendData: {});
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget renderExtend(RegisterExtendField e) {
    if (e is RegisterExtendTextField) {
      return Container(
        margin: const EdgeInsets.only(bottom: 10),
        child: dul.TextFieldWidget(
          controller: e.controller,
          hint: e.label,
          keyboardType: e.keyboardType,
          obscureText: e.obscureText,
        ),
      );
    }
    if (e is RegisterExtendDropdown) {
      return Container(
        padding: const EdgeInsets.only(left: 20, right: 20),
        margin: const EdgeInsets.only(bottom: 10),
        decoration: const ShapeDecoration(
          shape: RoundedRectangleBorder(
            side: BorderSide(
                width: 1.0, style: BorderStyle.solid, color: Colors.black54),
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
        ),
        width: double.infinity,
        child: DropdownButton(
          items: e.items,
          value: extendData[e.label],
          hint: Text(
            e.label,
            style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                  fontSize: 16,
                  color: Colors.black54,
                ),
          ),
          isExpanded: true,
          itemHeight: 48,
          elevation: 0,
          borderRadius: BorderRadius.circular(10),
          iconEnabledColor: widget.args.color,
          underline: Container(),
          onChanged: (onChanged) {
            // e.onChanged(e);
            extendData[e.label] = onChanged;
            setState(() {});
          },
        ),
      );
    }
    if (e is RegisterExtendRadio) {
      return Container(
        margin: const EdgeInsets.only(bottom: 10),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              e.label,
              style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                    fontSize: 16,
                    // color: Colors.white,
                  ),
            ),
            ...e.items.keys.map((label) {
              return ListTile(
                title: Text(label),
                leading: Radio<dynamic>(
                  value: e.items[label],
                  groupValue: extendData[e.label],
                  onChanged: (dynamic value) {
                    setState(() {
                      extendData[e.label] = value;
                    });
                  },
                ),
              );
            }).toList()
          ],
        ),
      );
    }
    if (e is RegisterExtendCheck) {
      return Container(
        margin: const EdgeInsets.only(bottom: 10),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              e.label,
              style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                    fontSize: 16,
                    // color: Colors.white,
                  ),
            ),
            ...e.items.keys.map((label) {
              return ListTile(
                title: Text(label),
                leading: Checkbox(
                  value: ((extendData[e.label] ?? []) as List)
                      .contains(e.items[label]),
                  onChanged: (dynamic value) {
                    setState(() {
                      if (extendData[e.label] == null) {
                        extendData[e.label] = [];
                      }
                      if (value) {
                        (extendData[e.label] as List).add(e.items[label]);
                      } else {
                        (extendData[e.label] as List).remove(e.items[label]);
                      }
                    });
                  },
                ),
              );
            }).toList()
          ],
        ),
      );
    }
    return Container();
  }
}
