import 'dart:async';

import 'package:flutter/material.dart';
import 'package:masdul_widget/textfield_widget.dart';

import 'arguments/search_page_arguments.dart';
import 'layout.dart';
import 'models/search_page_widget_model.dart';

class SearchPageWidget<T> extends StatefulWidget {
  final SearchPageArguments<T> args;

  const SearchPageWidget({
    super.key,
    required this.args,
  });

  @override
  State<SearchPageWidget<T>> createState() => _SearchPageWidgetState<T>();
}

class _SearchPageWidgetState<T> extends State<SearchPageWidget<T>> {
  bool isLoading = false;
  TextEditingController controller = TextEditingController();
  List<SearchPageWidgetModel<T>> data = [];
  List<SearchPageWidgetModel<T>> searchData = [];
  Timer? _debounce;

  @override
  void initState() {
    data = widget.args.data;
    searchData = data;
    controller.addListener(textListener);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    _debounce?.cancel();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant SearchPageWidget<T> oldWidget) {
    data = widget.args.data;
    searchData = data;
    controller.removeListener(textListener);
    controller.addListener(textListener);
    setState(() {});

    super.didUpdateWidget(oldWidget);
  }

  @override
  didChangeDependencies() {
    data = widget.args.data;
    searchData = data;
    setState(() {});

    super.didChangeDependencies();
  }

  void textListener() {
    _onChanged();
  }

  _onChanged() {
    if (_debounce?.isActive ?? false) _debounce!.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      // do something with query
      if (widget.args.isLocalSearch) {
        var text = controller.text;
        searchData = data.where((element) {
          bool titleFound =
              element.title.toLowerCase().contains(text.trim().toLowerCase());
          bool descFound = false;
          if (element.description == null) descFound = true;
          if (element.description != null) {
            if (element.description!.isEmpty) {
              descFound = true;
            } else {
              descFound = element.description!
                  .toLowerCase()
                  .contains(text.trim().toLowerCase());
            }
          }
          return titleFound || descFound;
        }).toList();
        setState(() {});
      }
      widget.args.onTextChanged(
        controller.text,
        widget.args.isLocalSearch
            ? searchData.map((e) => e.value).toList()
            : <T>[],
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Layout(
      isLoading: isLoading,
      child: Container(
        constraints: BoxConstraints(
          maxHeight: MediaQuery.of(context).size.height,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (widget.args.title != null)
              Text(
                widget.args.title ?? "",
                style: Theme.of(context)
                    .textTheme
                    .bodyLarge!
                    .copyWith(fontWeight: FontWeight.bold, fontSize: 16),
              ),
            if (widget.args.title != null) const SizedBox(height: 10),
            TextFieldWidget(
              controller: controller,
              suffix: const Icon(Icons.search),
              hint: widget.args.hint,
            ),
            const SizedBox(height: 10),
            Expanded(
              child: RefreshIndicator(
                onRefresh: () async {
                  widget.args.onTextChanged(
                    controller.text,
                    widget.args.isLocalSearch
                        ? searchData.map((e) => e.value).toList()
                        : <T>[],
                  );
                },
                child: ListView.builder(
                    itemCount: searchData.length,
                    itemBuilder: (context, index) {
                      var d = searchData[index];
                      return InkWell(
                        onTap: widget.args.onSelected == null
                            ? null
                            : () {
                                widget.args.onSelected!(d.value);
                              },
                        child: Card(
                          child: ListTile(
                            leading: d.leading,
                            trailing: d.trailing,
                            title: Text(d.title),
                            subtitle: d.description != null
                                ? Text(d.description!)
                                : null,
                          ),
                        ),
                      );
                    }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
